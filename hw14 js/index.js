
if (localStorage.getItem('style') === 'dark') {
    document.body.classList.toggle('dark');
  }

  const btnChangeColor = document.querySelector('#btn-change');
  btnChangeColor.addEventListener('click', function(){
    document.body.classList.toggle('dark');
    if (document.body.getAttribute('class') === 'dark') {
      localStorage.setItem('style', '');
    } else {
      localStorage.setItem('style', 'dark');
    }
   
  })

