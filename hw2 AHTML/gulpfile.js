import gulp from 'gulp';
import htmlmin from 'gulp-htmlmin';
import concat from 'gulp-concat';
import terser from 'gulp-terser';
import cleanCSS from 'gulp-clean-css';
import clean from 'gulp-clean-css';
import imagemin from 'gulp-imagemin';
import bs from 'browser-sync';

const browserSync = bs.create();

import dartSass from 'sass';
import gulpSass from 'gulp-sass';

const sass = gulpSass(dartSass);

function scss() {
    return gulp.src('./src/style/*')
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(gulp.dest('./dist/style'));
  };

  const cleanDist = () => {
    return gulp.src('./dist', { read: false })
        .pipe(clean());
}

const imgMin  = () => {
    return gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
}

const html = () => {
    return gulp.src('./src/**/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('./dist'))
}

const js = () => {
    return gulp.src('./src/**/*.js')
        .pipe(concat('index.js'))
        .pipe(terser({
            keep_fnames: true,
            mangle: false
        }))
        .pipe(gulp.dest('./dist/js'))
}

const css = () => {
    return gulp.src('./src/**/*.css')
        .pipe(concat('main.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist/css'))
}

const dev = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch('./src/**/*', gulp.series(cleanDist, gulp.parallel(html,scss,css,js), (next) => { 
        browserSync.reload(); 
        next();
    }))

}

gulp.task('img', imgMin);
gulp.task('scss', scss);

gulp.task('build', gulp.series(cleanDist, gulp.parallel(html,scss,css,js)));
gulp.task('dev', gulp.series('build', dev));




