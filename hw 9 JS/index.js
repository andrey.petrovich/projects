// Опишіть, як можна створити новий HTML тег на сторінці.

// за допомогою метода document.createElement

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

// insertAdjacentHTML дає змогу вставити код HTML в любе місце документа. Перший параметр insertAdjacentHTML це кодове слово куди вставляти відносно елемента

// Як можна видалити елемент зі сторінки?

// через метод .remove()


const cityArray = ["Vinnitsia", "Dnipro", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const numberArray = ["1", "2", "3", "4", "4", "5"];
 
const showListArray = (arr) => {
    const ul = document.createElement("ul");

    arr.forEach(element =>{
        const li = document.createElement("li");

        if(Array.isArray(element)){
            li.appendChild(showListArray(element))
        }else{
            li.innerText = element;
        }
        ul.appendChild(li);
    })
    return ul
}

document.body.appendChild(showListArray(cityArray));

document.body.appendChild(showListArray(numberArray));