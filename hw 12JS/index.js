// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Є деякі обмеження одне із них це неможливо відстежити введення даних


const showColorEnter = document.getElementById('btn-enter');
const showColorS = document.getElementById('btn-s');
const showColorE = document.getElementById('btn-e');
const showColorO = document.getElementById('btn-o');
const showColorN= document.getElementById('btn-n');
const showColorL = document.getElementById('btn-l');
const showColorZ = document.getElementById('btn-z');


document.addEventListener('keydown', (e) =>{

        if (e.key === 'Enter') {
            showColorEnter.style.backgroundColor = 'blue';
        } else if ((e.key != 'Enter')){
        showColorEnter.style.backgroundColor = '';
        } 
        
        if (e.key === 's') {
            showColorS.style.backgroundColor = 'blue';
        } else if ((e.key != 's')){
        showColorS.style.backgroundColor = '';}

        if (e.key === 'e') {
            showColorE.style.backgroundColor = 'blue';
        } else if ((e.key != 'e')){
        showColorE.style.backgroundColor = '';}

        if (e.key === 'o') {
            showColorO.style.backgroundColor = 'blue';
        } else if ((e.key != 'o')){
        showColorO.style.backgroundColor = '';}

        if (e.key === 'n') {
            showColorN.style.backgroundColor = 'blue';
        } else if ((e.key != 'n')){
        showColorN.style.backgroundColor = '';}

        if (e.key === 'z') {
            showColorZ.style.backgroundColor = 'blue';
        } else if ((e.key != 'z')){
        showColorZ.style.backgroundColor = '';}

        if (e.key === 'l') {
            showColorL.style.backgroundColor = 'blue';
        } else if ((e.key != 'l')){
        showColorL.style.backgroundColor = '';}
      });
      


