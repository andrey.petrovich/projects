const images = document.querySelectorAll(".img");
const firstImage = images[0];
const lastImage = images[images.length - 1];
const stopBtn = document.getElementById("stop");
const starBtn = document.getElementById("start");

const slider = () => {
  const currentImage = document.querySelector(".show");
  if (currentImage !== lastImage) {
    currentImage.classList.remove("show");
    currentImage.nextElementSibling.classList.add("show");
  } else {
    currentImage.classList.remove("show");
    firstImage.classList.add("show");
  }
};

let count = setInterval(slider, 3000);

starBtn.addEventListener("click", () => {
  count = setInterval(slider, 3000);
  
});

stopBtn.addEventListener("click", () => {
  clearInterval(count);
  
});
