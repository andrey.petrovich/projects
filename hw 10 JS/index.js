

let tabsTitle = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content");

tabsTitle.forEach(function(el) {
   el.addEventListener("click", openTabs);
});


function openTabs(el) {
   let btnTarget = el.currentTarget;
   let tabs = btnTarget.dataset.tabs;

   tabsContent.forEach(function(el) {
      el.classList.remove("active");
   });

   tabsTitle.forEach(function(el) {
      el.classList.remove("active");
   });

   document.querySelector("#" + tabs).classList.add("active");
   
   btnTarget.classList.add("active");
}

console.log(tabsTitle);
console.log(tabsContent);
